---
title: Software Carpentry Workshop
layout: event
organizers:
  - dolling
  - belayneh
lecturers:
  - dolling
  - belayneh
type:   workshop
start:
    date:   "2020-06-22"
    time:   "09:00"
end:
    date:   "2020-06-23"
    time:   "18:00"
location:
    campus: "Online Event"
fully_booked_out: false
registration_link: https://events.hifis.net/event/16/
registration_period:
    from:   "2020-05-26"
    to:     "2020-06-09"
excerpt:
    "This basic Software Carpentry workshop will teach Shell, Git and Python for scientists and PhD students."
---

## Goal

Introduce scientists and PhD students to a powerful toolset to enhance their
research software workflow.

## Content

A Software Carpentry workshop is conceptualized as a two-day event that covers
the basic tools required for a research software workflow:

* The _Shell_ as a foundation for the following tools
* Employing _Git_ as version control system (VCS)
* Introduction into the _Python_ programming language

Details can also be found directly at the
[GFZ event page](https://swc-bb.gitext-pages.gfz-potsdam.de/swc-pages/2020-06-22-virtual/).


## Requirements

Neither prior knowledge nor experience in those tools is needed.
A headset (or at least headphones) is required.
Two monitors are recommended.
