---
title: "HIFIS in the EGI newsletters"
title_image: default
date: 2020-06-08
authors:
  - jandt
layout: blogpost
categories:
  - news
excerpt_separator: <!--more-->
lang: en
lang_ref: 2020-06-19-hifis-in-the-egi-newsletter
---

_Helmholtz Federated IT Services - Promoting IT based science at all levels_,
is the title of our article that appeared in the newsletter of our friends 
from the EGI Federation. In the EGI Newsletters, we give an overview of the
new HIFIS platform.
<!--more-->

<a type="button" class="btn btn-outline-primary btn-lg" href="https://www.egi.eu/about/newsletters/helmholtz-federated-it-services-promoting-it-based-science-at-all-levels/">
  <i class="fas fa-external-link-alt"></i> Read more
</a>
