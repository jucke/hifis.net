---
title: Guidelines
title_image: default
layout: default
author: none

---

## HIFIS Suggestions and Guidelines
The optimal use of online collaboration tools is prerequisite for successful
research and especially in times of social distancing.
We provide guidelines and helping information.

<div class="flex-cards">
{%- assign posts = site.categories['guidelines'] | where: "lang", "en" -%}
{% for post in posts -%}
{% include cards/post_card_image.html post=post %}
{% endfor -%}
</div>
<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
