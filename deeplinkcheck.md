---
title: List of deep links
title_image: default
layout: default
excerpt:
    This is a list of published deep links to HIFIS and HIFIS documentation pages.
---

## Links to hifis.net

* <https://hifis.net/doc>
* <https://hifis.net/policies>
* <https://hifis.net/team>
* <https://hifis.net/contact>
* <https://hifis.net/roadmap>
* <https://hifis.net/guidelines>
* <https://hifis.net/news>
* <https://hifis.net/services>
* <https://hifis.net/mission/publications>
* <https://hifis.net/partners>


## Links to software.hifis.net

* <https://software.hifis.net>
* <https://software.hifis.net/services>
* <https://software.hifis.net/services/training>
* <https://software.hifis.net/services/helmholtzgitlab>
* <https://software.hifis.net/services/consulting>
* <https://software.hifis.net/blog>
* <https://software.hifis.net/events>



## Links to Technical documentation

* <https://hifis.net/doc/core-services/aai-proxy/>
* <https://hifis.net/doc/core-services/fts-endpoint/>
* <https://hifis.net/doc/service-portfolio/initial-service-portfolio/how-services-are-selected/#selected-services-for-initial-helmholtz-cloud-service-portfolio>
* <https://hifis.net/doc/service-integration/pilot-services/pilot-services/>
* <https://hifis.net/doc/backbone-aai/list-of-connected-centers/>
