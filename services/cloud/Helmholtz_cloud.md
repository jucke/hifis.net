---
title: Helmholtz Cloud 
title_image: background-cloud.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
excerpt: >-
    The Helmholtz Cloud offers IT services to all Helmholtz centres and the entire scientific community and partners.
---

<div class="image-block">
    <img
        class="help-image left"
        alt="Helmholtz Cloud Portal Screenshot"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal.jpg"
    />
    <p>
    <strong>In the <a href="https://cloud.helmholtz.de/">Helmholtz Cloud</a>, members of the Helmholtz Association of German research centers provide selected IT-Services for joint use.</strong><br>
    The Service Portfolio covers the whole scientific process and offers Helmholtz employees and their project partners a federated community cloud with uniform access for them to conduct and support excellent science. 
    The connection to international developments (e.g. EOSC at European level) is regarded as essential and the "FAIR" principles in particular are taken into account.
    </p>
</div>

# Federated scientific community cloud

Helmholtz Cloud is designed to make the IT services of the service providing Helmholtz Centres available across Helmholtz. Each service is provided by one or more centres, respectively, for the use by other Helmholtz Centres or cross-centre project groups. They also include the possibility of integrating external guests, so that even national and international scientific collaborations are easily possible.

# Seamless collaboration
The technical prerequisites for seamless login are provided by the common Authentication and Authorisation Infrastructure, the [Helmholtz AAI](https://aai.helmholtz.de/). With the Helmholtz AAI, the users of the Helmholtz Centres can use their home institution's login to use a Helmholtz Cloud Service. That means, only the home institution's login + password combination is needed to enter all Helmholtz Cloud services.
The process from a user's perspective is [visualized in this tutorial](https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html).

Furthermore, also invited guests from outside Helmholtz are able to use Helmholtz Cloud services, e.g. via their home organisation's login, or via public identity providers such as ORCID.

The organisational framework is provided by the [Helmholtz AAI Policies](https://hifis.net/doc/backbone-aai/policies/).

### Funded by the Helmholtz Association

Helmholtz Cloud is funded by the Helmholtz Association of German Research Centres. As a result, the use of Helmholtz Cloud services can be offered free of charge to Helmholtz users and their collaboration partners.

<h3>Further information</h3>
<div>
    <p>
    <a href="https://cloud.helmholtz.de">Helmholtz Cloud Portal</a>: Browse our current cloud services.
    </p>
    <p>
    <a href="Service_portfolio.html">The portfolio of the Helmholtz Cloud</a>
    </p>
    <p>
    <a href="cloud_login.html">Login and access to Helmholtz Cloud services</a>
    </p>
    <p>
    <a href="provider.html">Helmholtz Cloud Service Provider</a>
    </p>
</div>

<div>
<h3>Feedback and Support</h3>
    <p>
    We welcome your suggestions, tips and support. They help us to become better. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
</div>
