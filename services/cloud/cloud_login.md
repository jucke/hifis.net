---
title: Service Login & Access
title_image: cory-billingsley-PQ36AkKJfAc-unsplash.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
excerpt: >-
    Login and access to Helmholtz Cloud services.
---

<h1>Helmholtz Cloud Portal</h1>
<div class="image-block">
    <img
        class="help-image right"
        alt="Helmholtz Cloud Service Description"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal.jpg"
        style="max-width: 28rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>
        The <a href="https://cloud.helmholtz.de">Helmholtz Cloud Portal</a> mediates the user's access to all Helmholtz Cloud Services.
        </p>
        <p>
        To select a service, visit the Cloud Portal.
        </p>
    </div>
</div>

<h3>Login to Helmholtz Cloud Service</h3>
<div class="image-block">
    <img
        class="help-image right"
        alt="Helmholtz Cloud Service Description"
        src="{{ site.directory.images | relative_url }}/services/Cloud_Portal_Services.jpg"
        style="max-width: 28rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>
        Helmholtz AAI allows seamless access to a large and growing number of Helmholtz Cloud services. You can use your home institution’s login to use the services seamlessly.
        </p>
        <p>
        The tutorial <a href="https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html">Login to Helmholtz Cloud and associated services via Helmholtz AAI</a> explains the process click-by-click.
        </p>
    </div>
</div>

<h3>Getting Access to a Service</h3>
<div class="image-block">
    <img
        class="help-image right"
        alt="Helmholtz Cloud Service Description"
        src="{{ site.directory.images | relative_url }}services/Cloud_Portal_Service_Description.jpg"
        style="max-width: 28rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>
        The <a href="https://cloud.helmholtz.de">Helmholtz Cloud</a> is currently being established. Numerous services can already be used in pilot status.
        </p>
        <p>
        If you would like to use a service that is not yet available to you, please write to the contact given in the detailed service description.
        </p>
    </div>
</div>

<h3>Further information</h3>
<div>
    <p>
    <a href="https://cloud.helmholtz.de">Helmholtz Cloud Portal</a>: Browse our current cloud services.
    </p>
    <p>
    <a href="Helmholtz_cloud.html">About the Helmholtz Cloud</a>
    </p>
    <p>
    <a href="Service_portfolio.html">The portfolio of the Helmholtz Cloud</a>
    </p>
    <p>
    <a href="provider.html">Helmholtz Cloud Service Provider</a>
    </p>
</div>

<h3>Feedback and Support</h3>
<div>
    <p>
    We welcome your suggestions, tips and support. They help us to become better. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
</div>
