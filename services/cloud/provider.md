---
title: Helmholtz Cloud Service Provider
title_image: Plony.jpg
layout: services/default
author: none
additional_css:
    - services/services-page-images.css
excerpt: >-
    How to hand in new service to be offered in Helmholtz Cloud.
---


<h1>Becoming Service Provider</h1>
<div class="image-block">
    <img
        class="help-image right"
        alt="Screenshot of plony.helmholtz-berlin.de"
        src="{{ site.directory.images | relative_url }}/services/Plony.jpg"
        style="max-width: 28rem !important;min-width:  5rem !important;"
    />
    <div style="flex: 1 0 0;">
        <p>
        As a service provider, you can hand in your services for Helmholtz Cloud at any time! The Application form for potential Helmholtz Cloud Services is online available in Plony. Check out at <a href="https://plony.helmholtz-berlin.de/">plony.helmholtz-berlin.de</a>.
        </p>
        <p>
        You find an instruction on the Application Form on the landing page of Plony.
        </p>
    </div>
</div>



#### Features of Plony in general and Application Form specifically:


* Connected to Helmholtz AAI – simply login with the credentials of your home institution.
* Flexible in use – you can start filling your Application Form, save it and continue whenever you want without loosing information already typed in.
* Guiding through selection criteria – exclusion criteria are built into the Application form and validation functionality allows you to check whether you fulfill all required criteria.
* Transparency in every status – using the overview “My Services” you can see all your applications sorted by status. To improve transparency about applications within your Helmholtz center, you can even see sent applications filled out by colleagues from your Helmholtz center.


#### As soon as you sent your application to HIFIS, we will check it and give you feedback:


* Either there are some questions/things need to be clarified and we’re giving the application back to you, or
* everything is fine and your service is added to the Helmholtz Cloud Service Portfolio. In this case we will ask you to give us some more information by filling out the service canvas.



<div>
    <p>
    If you have questions need assistance, please do not hesitate to contact. Please write to us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
    <p>
    Information about the service onboarding process, evaluation and exclusion criteria can be found <a href="https://www.hifis.net/doc/service-portfolio/">in our documentation</a>.
    </p>
</div>

<h3>Further information</h3>
<div>
    <p>
    <a href="https://cloud.helmholtz.de">Helmholtz Cloud Portal</a>: Browse our current cloud services.
    </p>
    <p>
    <a href="Helmholtz_cloud.html">About the Helmholtz Cloud</a>
    </p>
    <p>
    <a href="Service_portfolio.html">The portfolio of the Helmholtz Cloud</a>
    </p>
    <p>
    <a href="cloud_login.html">Login and access to Helmholtz Cloud services</a>
    </p>
</div>

<div>
<h3>Feedback and Support</h3>
    <p>
    We welcome your suggestions, tips and support. They help us to become better. Please contact us at <a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a>.
    </p>
</div>
