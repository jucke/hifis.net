---
title: <i class="fas fa-question-circle"></i> FAQ - Frequently Asked Questions
title_image: default
layout: default
excerpt:
    Collection of Frequently Asked Question (FAQ) about HIFIS.
---

<div class="alert alert-success">
    <h1 id="motivation" class="alert-heading">Motivation for HIFIS</h1>
    <p>
        The top position of Helmholtz research is increasingly based on cross-centre and international cooperation and
        common access to data treasure and -services. At the same
        time the significance of a sustainable software development for the research process is recognised.</p>
    <p>
        HIFIS aims to ensure an excellent information environment for outstanding research in all Helmholtz research
        fields and a seamless and performant IT-infrastructure connecting knowledge from all centres. It will build a
        secure and easy-to-use collaborative environment with efficiently accessible ICT services from anywhere. HIFIS
        will also support the development of research software with a high level of quality, visibility and
        sustainability.</p>
    <p> Documentation, Full Proposal texts and Publications <a href="{% link publications.md %}">can be found here</a>.
    </p>
</div>

## General
{:.text-success}

#### [What is the mission of HIFIS?][1]
The Helmholtz Infrastructure for Federated ICT Services (HIFIS) ensures an
excellent information environment for outstanding research in all
Helmholtz research fields.

<i class="fas fa-headphones"></i>
**You are invited to [tune in to the recent Resonator podcast on HIFIS](https://resonator-podcast.de/2021/res172-hifis/), to get an idea on the our aims and scope!** (German only)
<i class="fas fa-headphones"></i>

#### [How can HIFIS help me as a researcher?][2]
The purpose of HIFIS is to support science and scientists. Within the HIFIS platform, we are building:
- The [**Helmholtz Cloud**](https://cloud.helmholtz.de), providing a portfolio of seamlessly accessible IT services to simplify your daily work.
- All cloud services and software trainings and support are free of charge!
- A **Helmholtz-wide login mechanism**, the [Helmholtz AAI](https://aai.helmholtz.de),
- A stable, high-bandwidth **network infrastructure** connecting all Helmholtz centers,
- [**Software services**]({% link services/index.md %}#software) to provide you with a common platform,
  [training]({% link events.md %}) and
  [support]({% link services/software/consulting.html %}) for high-quality sustainable software development.


#### [How can I access Cloud Services?][7]
It is easier than ever before, since you do not need to create new accounts with new passwords!
Just search for "Helmholtz AAI" when logging in and use your home institute's credentials.

Please
[**refer to our pictured tutorial on how to access Cloud Services via Helmholtz AAI**](https://aai.helmholtz.de/tutorial/2021/06/23/how-to-helmholtz-aai.html)
for more details.

#### [When will all of this take place?][3]
The HIFIS platform was set up in 2019, with basic infrastructure services (AAI) and pilot services online in 2020. The initial cloud service portfolio has been made public by
end of 2020. 
The full version of the Helmholtz cloud is expected to be available by end 2021 with the release of the cloud portal and finalisation of the cooperation agreement.
Please also refer to our [**HIFIS Roadmap**](roadmap) to stay up to date.
The 
[Software Services Training]({% link events.md %})
and 
[Support platform]({% link services/software/consulting.html %}) is already available.

#### [Anything I can do to get things done faster?][4]
We have several pilot cloud services and installations running. Please [contact us](mailto:{{ site.contact_mail }}) if you need urgent assistance.

#### [Where can I register for a newsletter?][6]
We do not send newsletters, but you are invited to register for our Announcement letter `announce@hifis.net` to never miss any upcoming events!

Either [**register by clicking on this link** and sending a registration mail](mailto:sympa@desy.de?subject=sub%20hifis-announce) or 
[**register here** - you can also unregister here](https://lists.desy.de/sympa/subscribe/hifis-announce).

To stay updated on general HIFIS news, you are invited to check out 
[our RSS news feed]({{ site.feed.collections.posts.path | relative_url }}) - 
besides, of course, regularly checking our website and [**news section!**](news)



#### [My Helmholtz center is not directly involved into HIFIS. Do you still help me?][5]
Yes, of course. HIFIS is a Helmholtz-wide platform that aims to
provide offers for **all** Helmholtz centers. 
Please [contact us](mailto:{{ site.contact_mail }}) or any of the [HIFIS team members]({% link team.md %}) to get assistance.


#### [How should I acknowledge HIFIS assistance in a publication?][10]

Please include the following text:

> We gratefully acknowledge the HIFIS (Helmholtz Federated IT Services) 
> team for ... (e.g. support with the [components above][2])



[1]: #what-is-the-mission-of-hifis
[2]: #how-can-hifis-help-me-as-a-researcher
[3]: #when-will-all-of-this-take-place
[4]: #anything-i-can-do-to-get-things-done-faster
[5]: #my-helmholtz-center-is-not-directly-involved-into-hifis-do-you-still-help-me
[6]: #where-can-i-register-for-a-newsletter
[7]: #how-can-i-access-cloud-services
[10]: #how-should-i-acknowledge-hifis-assistance-in-a-publication
