---
title: The Team
title_image: default
layout: team
additional_css:
    - frontpage.css
excerpt: A list of HIFIS team members.
---
{%- comment -%}
  This markdown file triggers the generation of the team page.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{%- endcomment -%}
