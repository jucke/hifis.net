---
date: 2020-10-01
title: Tasks in October 2020
service: software
---

## Reusable Ansible recipes for a scalable GitLab instance
The future software management platform will be based on reusable Ansible recipes.
This allows other research centers to reuse the implemented solution.
The open-source recipes are publicly available on [gitlab.com/hifis/ansible][gitlab-ansible].

[gitlab-ansible]: https://gitlab.com/hifis/ansible
