---
date: 2022-01-01
title: Tasks in Jan 2022
service: cloud
---

## Initial Review of Cloud Service Portfolio
The current set of cloud services will be reviewed. The results will enter the annual HIFIS report and further service portfolio maintenance.
