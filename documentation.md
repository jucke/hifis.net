---
title: HIFIS Documentation
title_image: default
layout: default
excerpt:
    HIFIS Technical and Administrative Documentation, Policies and Internal Repositories.
---
# Technical and Administrative Documentation

All information on the architectural components and processes related to HIFIS and Helmholtz Authentication and Authorisation Infrastructure (AAI) are collected in the 
[**HIFIS Documentation** at hifis.net/doc](https://hifis.net/doc/). 
Some frequently used documentation links are listed below:

#### Guidelines and Policies
* [Helmholtz AAI Policies](https://aai.helmholtz.de/policies)
* [Helmholtz AAI Concepts](https://aai.helmholtz.de/concept)
* Guidelines for [Managers of Virtual Organisations](https://hifis.net/doc/backbone-aai/guidelines-vos/)
* Guidelines for [Service Providers](https://hifis.net/doc/backbone-aai/guidelines-services/)
* How to [use HIFIS Transfer Service](https://hifis.net/doc/core-services/fts-endpoint/)

#### Service Usage
* [Exemplary usage plots](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci/-/blob/master/example_graphs.md) for AAI usage, Helmholtz Codebase Service, and HIFIS Software Course Participation. More to come!

#### Documentation on the Current Status of Implementation

* [List of Services in Cloud Service Portfolio: Pipeline and Current Catalogue](https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/)
* [Technical Documentation on Integrated Cloud Services](https://hifis.net/doc/cloud-services/)
* [Pilot and Associated Cloud Services](https://hifis.net/doc/service-integration/pilot-services/pilot-services/) (selection)
* [List of Helmholtz Centres](https://hifis.net/doc/backbone-aai/list-of-connected-centers/) that are technically connected to the Helmholtz AAI
* [List of supported Virtual Organisations](https://hifis.net/doc/backbone-aai/list-of-vos/)

#### Public Gitlab Repositories
The following documentation projects are publicly accessible for reading. You can contribute to them after signing in via Helmholtz AAI.
* [Gitlab Project for this homepage](https://gitlab.hzdr.de/hifis/hifis.net)
* [Gitlab Project for HIFIS Documentation](https://gitlab.hzdr.de/hifis/hifis-technical-documentation)
* [Gitlab Project for AAI Website](https://gitlab.hzdr.de/helmholtz-aai/aai.helmholtz.de)

#### Governance
* Original [platform proposal for HIFIS](https://www.helmholtz.de/fileadmin/user_upload/01_forschung/Helmholtz_Inkubator_HIFIS.pdf)
* [The Helmholtz Incubator](https://www.helmholtz.de/en/research/information-data-science/helmholtz-incubator/)

---

# Internal Documents
The access to the following documents (drafts) is currently restricted to HIFIS members. 

#### Technical and Administative Documentation
* [Draft of HIFIS Infrastructure Policies Draft: Sources](https://nubes.helmholtz-berlin.de/f/82025317)
* [Draft of Helmholtz Cloud Contracts](https://gitlab.hzdr.de/helmholtz-cloud-contracts/helmholtz-cloud-contract/-/tree/master)

#### Other presentations
* [10th Incubator Workshop, April 2021](https://nubes.helmholtz-berlin.de/f/254662564)
* [9th Incubator Workshop, Nov 2020](https://nubes.helmholtz-berlin.de/f/155732886)
* [HIFIS presentation at KoDa Meeting, October 2020](https://nubes.helmholtz-berlin.de/f/149595105)
* [8th Incubator Workshop, June 2020](https://nubes.helmholtz-berlin.de/f/84128581) - [PDF version](https://nubes.helmholtz-berlin.de/f/84141777)
* [Scientific Advisory Board, April 2020](https://nubes.helmholtz-berlin.de/f/79099187)

#### Reports
* [Annual report for the year 2020](https://nubes.helmholtz-berlin.de/f/174628359)
* [Annual report for the year 2019](https://nubes.helmholtz-berlin.de/f/78483551)
* [Feedback Report of the Scientific Advisory Board, May 2020](https://nubes.helmholtz-berlin.de/f/84005457)

#### Internal Organization
* [HIFIS Stakeholder List](https://nubes.helmholtz-berlin.de/f/41769740)
* [HIFIS Working Groups](https://gitlab.hzdr.de/hifis/communication/hifis-structure/-/blob/master/hifis_structure.md) (to be updated)
